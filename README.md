# TestAngularFe

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.16.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


# Untuk setup Mock Data

1. Open folder mockup-be
2. Setup environment sesuai dengan database yang ada dilocal
3. Jalankan command diterminal "composer install"
4. Setelah selesai jalankan command "php artisan migrate" dan "php artisan db:seed"
5. Saat sukses jalankan command "php -S localhost:8000 -t public"

# Untuk setup Front End
1. Setup env ke "localhost:8000"
2. Jalankan command "npm i"
3. Jalankan command "ng s -o"
4. user login "admin" password "admin"
