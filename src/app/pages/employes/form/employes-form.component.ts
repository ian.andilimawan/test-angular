import { Component, OnInit } from '@angular/core';
import { Datas, Employes, EmployesData, IEmployes } from '../model/employes.model';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployesService } from '../service/employes.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { EmployesFormGroup, createEmployesFormGroup } from '../model/employes-form.model';
import { Location } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-employes-form',
  templateUrl: './employes-form.component.html',
  styleUrls: ['./employes-form.component.scss']
})
export class EmployesFormComponent implements OnInit {
  data?: Datas
  view: 'detail' | 'edit' | 'new' = 'new'
  title: string = ''
  formBuilder = new FormBuilder();

  listStatusType = [
    {value: "1", label: 'Active'},
    {value: "0", label: 'Non-Active'}
  ]

  editForm: EmployesFormGroup = createEmployesFormGroup(this.formBuilder, {
    id: this.formBuilder.control(null),
    username: this.formBuilder.control(null),
    firstname: this.formBuilder.control(null),
    lastname: this.formBuilder.control(null),
    email: this.formBuilder.control(null),
    birthdate: this.formBuilder.control(null),
    basic_salary: this.formBuilder.control(null),
    status: this.formBuilder.control(null),
    group: this.formBuilder.control(null),
    description: this.formBuilder.control(null),
  });


  constructor(
    protected activatedRoute: ActivatedRoute,
    private employeService: EmployesService,
    private route: Router,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ employes, view }) => {
      this.view = view
      this.updateForm(employes)
      this.updateTitle();
    });
  }

  private updateForm(res: Datas): void {
    this.data = res
    const data = Array.isArray(res.data) ? res.data : [res.data]

    this.editForm.reset({
      id: data[0]?.id,
      username: data[0]?.username,
      firstname: data[0]?.firstname,
      lastname: data[0]?.lastname,
      email: data[0]?.email,
      birthdate: data[0]?.birthdate,
      basic_salary: data[0]?.basic_salary,
      status: data[0]?.status,
      group: data[0]?.group,
      description: data[0]?.description,
    });

    if (this.view === 'detail') {
      Object.keys(this.editForm.controls).forEach(ctrl => {
        this.editForm.get(ctrl)?.disable()
      });
    }
  }

  private createFromForm(): EmployesData {
    return {
      id: this.editForm.get(['id'])!.value,
      username: this.editForm.get(['username'])!.value,
      firstname: this.editForm.get(['firstname'])!.value,
      lastname: this.editForm.get(['lastname'])!.value,
      email: this.editForm.get(['email'])!.value,
      birthdate: this.editForm.get(['birthdate'])!.value,
      basic_salary: this.editForm.get(['basic_salary'])!.value,
      status: this.editForm.get(['status'])!.value,
      group: this.editForm.get(['group'])!.value,
      description: this.editForm.get(['description'])!.value,
    }
  }

  previousState(): void {
    this.location.back();
  }

  private onSaveError(): void {
    this.route.navigateByUrl('employes')
  }

  private onSaveSuccess(): void {
    this.location.back();
  }

  private onSaveFinalize(): void {
    this.onSaveSuccess()
  }

  private subscribeToSaveResponse(result: Observable<HttpResponse<EmployesData>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: (res) => {
        this.onSaveSuccess()
      },
      error: () => this.onSaveError(),
    });
  }

  save(): void {
    const data = this.createFromForm();

    if (data.id !== null) {
      this.subscribeToSaveResponse(this.employeService.update(data));
    } else {
      this.subscribeToSaveResponse(this.employeService.create(data));
    }
  }

  private updateTitle(): void {
    switch (this.view) {
      case 'detail':
        this.title = 'Detail Employe'
        break;
      case 'edit':
        this.title = 'Edit Employe'
        break;
      default:
        this.title = 'New Employe'
        break;
    }
  }
}
