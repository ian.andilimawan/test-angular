export interface IEmployesFilter {
  id?: string;
  search?: string;
  searchStatus?: string;
}

export class EmployesFilter implements IEmployesFilter {
  constructor(
    public id?: string,
    public search?: string,
    public searchStatus?: string,
  ) {}
}
