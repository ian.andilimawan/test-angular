import { AbstractControl, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { EmployesData, IEmployes } from "./employes.model";

interface ControlWithGeneric<T> extends AbstractControl {
  value: T;
}

type EmployesFormGroupContent = {
  id: ControlWithGeneric<EmployesData['id']>;
  username: ControlWithGeneric<EmployesData['username']>;
  firstname: ControlWithGeneric<EmployesData['firstname']>;
  lastname: ControlWithGeneric<EmployesData['lastname']>;
  email: ControlWithGeneric<EmployesData['email']>
  birthdate: ControlWithGeneric<EmployesData['birthdate']>
  basic_salary: ControlWithGeneric<EmployesData['basic_salary']>
  status: ControlWithGeneric<EmployesData['status']>
  group: ControlWithGeneric<EmployesData['group']>
  description: ControlWithGeneric<EmployesData['description']>
};

export type EmployesFormGroup = FormGroup & EmployesFormGroupContent;

export function createEmployesFormGroup(
  fb: FormBuilder,
  controls: Partial<EmployesFormGroupContent> = {}
): EmployesFormGroup {
  const defaultControls: EmployesFormGroupContent = {
    id: fb.control(null),
    username: fb.control(null),
    firstname: fb.control(null),
    lastname: fb.control(null),
    email: fb.control(null),
    birthdate: fb.control(null),
    basic_salary: fb.control(null),
    status: fb.control(null),
    group: fb.control(null),
    description: fb.control(null),
  };

  const mergedControls = { ...defaultControls, ...controls };
  return fb.group(mergedControls) as EmployesFormGroup;
}
