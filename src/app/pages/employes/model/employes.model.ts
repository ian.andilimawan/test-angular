import { IEntityMain } from "src/app/utils/request/entity-main.model"

export interface IEmployes extends IEntityMain {
  valid?:   boolean;
  message?: string;
  data?: Datas[];
}

export interface Datas {
    current_page?:   number;
    data?:           EmployesData[];
    first_page_url?: string;
    from?:           number;
    last_page?:      number;
    last_page_url?:  string;
    next_page_url?:  string;
    path?:           string;
    per_page?:       number;
    prev_page_url?:  null;
    to?:             number;
    total?:          number;
}

export interface EmployesData {
  id?: number,
  username?: string
  firstname?: string
  lastname?: string
  email?: string
  birthdate?: Date | string
  basic_salary?: number
  status?: string
  group?: string
  description?: Date | string
}

export class Employes {
  constructor(
    public id?: number,
    public username?: string,
    public firstname?: string,
    public lastname?: string,
    public email?: string,
    public birthdate?: Date | string,
    public basic_salary?: number,
    public status?: string,
    public group?: string,
    public description?: Date | string,
  ) {}
}

export function getEmployesIdentifier(res: IEmployes): number | undefined {
  return res.id;
}
