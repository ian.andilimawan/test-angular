import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployesComponent } from './list/employes.component';
import { EmployesFormComponent } from './form/employes-form.component';
import { EmployesResolver } from './service/employes.resolver';

const routes: Routes = [
  {
    path: '',
    data: {
      defaultSort: 'id,desc',
    },
    component: EmployesComponent
  },
  {
    path: 'new',
    data: {
      view: 'new'
    },
    component: EmployesFormComponent,
    resolve: {
      employes: EmployesResolver
    }
  },
  {
    path: ':id/view',
    data: {
      view: 'detail'
    },
    component: EmployesFormComponent,
    resolve: {
      employes: EmployesResolver
    }
  },
  {
    path: ':id/edit',
    data: {
      view: 'edit'
    },
    component: EmployesFormComponent,
    resolve: {
      employes: EmployesResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployesRoutingModule { }
