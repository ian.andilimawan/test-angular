import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Employes, EmployesData } from '../model/employes.model';
import { EmployesService } from './employes.service';
import { HttpResponse } from '@angular/common/http';
import { EMPTY, Observable, of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class EmployesResolver implements Resolve<EmployesData | null> {
  constructor(private employesService: EmployesService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<EmployesData | null> {
    const id = Number(route.paramMap.get('id')!);

    // ! EDIT/DETAIL
    if (id) {
      return this.employesService.find(id).pipe(
        mergeMap((res: HttpResponse<Employes>) => {
          if (res.body) {
            return of(res.body);
          } else {
            this.router.navigate(['error/404']);
            return EMPTY;
          }
        }),
        catchError(() => {
          return EMPTY;
        })
      );
    }

    // ! CREATE NEW
    return of(new Employes());
  }
}
