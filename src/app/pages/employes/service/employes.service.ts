import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IEmployes, getEmployesIdentifier } from '../model/employes.model';
import { IResponse } from 'src/app/utils/request/response-list.model';
import { createRequestOption } from 'src/app/utils/request/request-util';
import { Pagination } from 'src/app/utils/request/request.model';

export type EntityResponseType = HttpResponse<IEmployes>;
export type EntityArrayResponseType = HttpResponse<IEmployes>

@Injectable({
  providedIn: 'root'
})
export class EmployesService {
  private url = `${environment.base_url}/employes`

  constructor(
    private http: HttpClient
  ) { }

  create(data: {}): Observable<HttpResponse<{}>> {
    return this.http.post(`${this.url}`, data, { observe: 'response' })
  }

  update(data: IEmployes): Observable<EntityResponseType> {
    return this.http.put<IEmployes>(`${this.url}/${getEmployesIdentifier(data) as number}`, data, { observe: 'response' })
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEmployes>(`${this.url}`, { params: options, observe: 'response' })
  }

  find(id: number, req?: Pagination): Observable<EntityResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEmployes>(`${this.url}/${id}/`, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.url}/${id}`, { observe: 'response' })
  }
}
