import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployesRoutingModule } from './employes-routing.module';
import { NgbModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { EmployesComponent } from './list/employes.component';
import { EmployesFormComponent } from './form/employes-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmployesResolver } from './service/employes.resolver';


@NgModule({
  declarations: [
    EmployesComponent,
    EmployesFormComponent
  ],
  imports: [
    CommonModule,
    EmployesRoutingModule,
    FormsModule,
    NgbModule,
    NgbPaginationModule,
    ReactiveFormsModule
  ],
  providers: [
    EmployesResolver
  ]
})
export class EmployesModule { }
