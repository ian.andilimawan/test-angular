import { Component, OnInit } from '@angular/core';
import { Datas, EmployesData, IEmployes } from '../model/employes.model';
import { BehaviorSubject, Observable, Subject, combineLatest, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { IResponse } from 'src/app/utils/request/response-list.model';
import { EmployesService } from '../service/employes.service';
import { ASC, DESC, ITEMS_PER_PAGE, SORT } from 'src/app/utils/request/pagination.constants';
import { Pagination } from 'src/app/utils/request/request.model';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployesFilter, IEmployesFilter } from '../model/employes-filter.model';
import { debounceTime, switchMap, tap } from 'rxjs/operators';
import { LoaderService } from 'src/app/utils/loader.service';

@Component({
  selector: 'app-employes',
  templateUrl: './employes.component.html',
  styleUrls: ['./employes.component.scss']
})
export class EmployesComponent implements OnInit {
  list: IEmployes = {}
  listDatas$ = new BehaviorSubject<Datas[]>([]);
  totalItems = 0;
  itemsPerPage: number = 10;
  page: number = 1;
  ngbPaginationPage = 1;
  predicate!: string;
  ascending!: boolean;

  listStatusType = [
    {value: "1", label: 'Active'},
    {value: "0", label: 'Non-Active'}
  ]

  filter: IEmployesFilter = new EmployesFilter();
  filterUpdate$: Subject<IEmployesFilter> = new Subject();

  constructor(
    private employeService: EmployesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public loaderService: LoaderService
  ) {
    this.filter.search = this.activatedRoute.snapshot.queryParams['search'] ?? undefined;

    this.filter.searchStatus = this.activatedRoute.snapshot.queryParams['searchStatus'] ?? undefined;

    this.itemsPerPage = this.activatedRoute.snapshot.queryParams['limit']
      ? Number(this.activatedRoute.snapshot.queryParams['limit'])
      : ITEMS_PER_PAGE;
   }

  ngOnInit(): void {
    this.handleNavigation()

    this.filterUpdate$
      .pipe(
        debounceTime(500),
        tap(() => (this.loaderService.showLoader())),
        switchMap(() => {
          const pageToLoad: number = this.page ?? 1;
          return this.observableTable(pageToLoad);
        })
      )
      .subscribe({
        next: res => {
          this.loaderService.hideLoader()
          this.onSuccess(res.body, res.headers, 1, true);
        },
        error: () => {
          this.loaderService.hideLoader()
          this.onError();
        },
        complete: () => {this.loaderService.hideLoader()}
      });
  }

  private observableTable(page?: number): Observable<HttpResponse<IEmployes>> {
    const pageToLoad: number = page ?? this.page ?? 1;

    const request = this.mappingRequestParam({
      page: pageToLoad,
      limit: this.itemsPerPage,
      searchStatus: this.filter.searchStatus ?? '',
      search: this.filter.search ?? '',
    });

    return this.employeService.query(request);
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? ASC : DESC)];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private mappingRequestParam(request: Pagination): Pagination {
    if (this.filter.search) {
      request = { ...request, ...{ 'search': this.filter.search } };
    }

    if (this.filter.searchStatus) {
      request = { ...request, ...{ 'searchStatus': this.filter.searchStatus } };
    }

    return request;
  }

  loadPage(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page ?? this.page ?? 1;
    this.observableTable(pageToLoad).subscribe({
      next: (res: HttpResponse<IEmployes>) => {
        this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate);
      },
      error: () => {
        this.onError();
      }
    })
  }

  onError() {

  }

  private onSuccess(data: IEmployes | null, headers: HttpHeaders, page: number, navigate: boolean): void {

    if (data?.data) {
      const dataArray = Array.isArray(data.data) ? data.data : [data.data];
      this.page = page;
      this.ngbPaginationPage = this.page;
      this.totalItems = dataArray[0].total ?? 0
      if (navigate) {
        this.router.navigate(['/employes'], {
          queryParams: {
            page: this.page,
            limit: this.itemsPerPage,
            search: this.filter.search ?? null,
            searchStatus: this.filter.searchStatus ?? null,
            sort: this.predicate + ',' + (this.ascending ? ASC : DESC),
          },
        });
      }
      this.list = data ?? {}
      this.listDatas$.next(dataArray)
      this.ngbPaginationPage = this.page;
    }
  }


  private handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap]).subscribe(([data, params]) => {
      const page = params.get('page');
      const pageNumber = +(page ?? 1);

      const sort = (params.get(SORT) ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === ASC;

      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    });
  }

  delete(res: IEmployes): void {
    this.employeService.delete(Number(res.id)).subscribe(
      (resp) => {
        this.loadPage()
      },
      (err) => {
        console.log(err);
      }
    )
  }

  onFilterChanged(): void {
    this.filterUpdate$.next(this.filter);
  }
}
