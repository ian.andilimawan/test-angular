import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private url = environment.base_url

  constructor(
    private http: HttpClient
  ) { }

  postData(data: []): Observable<HttpResponse<{}>> {
    return this.http.post(`${this.url}/login`, data, { observe: 'response' })
  }
}
