import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';
import { LoaderService } from 'src/app/utils/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = this.formBuilder.group({
    user: [null, [Validators.required]],
    password: [null, [Validators.required]]
  })

  constructor(
    private formBuilder: FormBuilder,
    private route: Router,
    private loginService: LoginService,
    public loaderService: LoaderService
  ) { }

  ngOnInit(): void {
  }

  onSuccess(): void {
    this.route.navigateByUrl('employes')
  }

  doLogin() {
    this.loaderService.showLoader()
    if (!this.loginForm.invalid) {
      const resp = this.loginService.postData(this.loginForm.value).subscribe(
        (succ) => {
          this.loaderService.hideLoader()
          this.onSuccess()
        },
        (err) => {
          alert(err.message)
          this.loaderService.hideLoader()

        }
      )
    }

  }
}
