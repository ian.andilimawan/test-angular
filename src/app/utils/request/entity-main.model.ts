export interface IEntityMain {
  id?: number;
  created_at?: Date | null;
  updated_at?: Date | null;
  deleted_at?: Date | null;
}

export class EntityMain implements IEntityMain {
  constructor(
    public id?: number,
    public created_at?: Date | null,
    public updated_at?: Date | null,
    public deleted_at?: Date | null,
  ) {}
}