export interface Pagination {
  resolve?: number;
  resolveList?: number;
  offset?: number;
  limit?: number;
  sort?: string | string[];
  sortOrder?: number;
  searchIsActive?: number;
  [key: string]: any;
}