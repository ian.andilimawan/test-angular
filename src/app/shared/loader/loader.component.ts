import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/utils/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {

  constructor(public loaderService: LoaderService) {}

  startLoading() {
    this.loaderService.showLoader();
  }

  stopLoading() {
    this.loaderService.hideLoader();
  }

}
