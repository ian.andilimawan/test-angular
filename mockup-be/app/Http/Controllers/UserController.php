<?php

namespace App\Http\Controllers;

use App\Models\Employer;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index(Request $request)
    {
        $users = User::where('name', $request->input('user'))->where('password', $request->input('password'))->first();

        if ($users === null) {
            return response()->json(['valid' => false, 'message' => 'data not found', 'data' => null], 404);
        }

        return response()->json(['valid' => true, 'message' => 'data retrieved successfully', 'data' => $users]);
    }
}
