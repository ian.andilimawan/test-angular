<?php

namespace App\Http\Controllers;

use App\Models\Employer;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EmployerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index(Request $request)
    {
        if (empty($request['limit'])) {
            $request['limit'] = 10;
        }

        if (!empty($request['search'])) {
            $keyword = $request->input('search');

            $users = Employer::where(function ($query) use ($keyword) {
            $columns = ['username', 'firstname', 'lastname', 'email', 'birthdate', 'basic_salary', 'status', 'group',
            'description'];
            foreach ($columns as $column) {
                $query->orWhere($column, 'like', '%' . $keyword . '%');
            }
            })->paginate($request->input('limit'));
            return response()->json(['valid' => true, 'message' => 'data retrieved successfully', 'data' => $users]);
        }

        if (($request['searchStatus']) == "1" || $request['searchStatus'] == "0") {
            $users = Employer::where('status', strval($request['searchStatus']))->paginate($request['limit']);
            return response()->json(['valid' => true, 'message' => 'data retrieved successfully', 'data' => $users]);
        }

        $users = Employer::paginate($request['limit']);

        return response()->json(['valid' => true, 'message' => 'data retrieved successfully', 'data' => $users]);
    }

    public function addEmployer(Request $requets) {

        try {
            $data = Employer::create([
                'username' =>  $requets->input('username'),
                'firstname' =>  $requets->input('firstname'),
                'lastname' =>  $requets->input('lastname'),
                'email' =>  $requets->input('email'),
                'birthdate' =>  $requets->input('birthdate'),
                'basic_salary' =>  $requets->input('basic_salary'),
                'group' =>  $requets->input('group'),
                'status' =>  $requets->input('status'),
                'description' =>  $requets->input('description'),
            ]);

            return response()->json(['valid' => true, 'message' => 'success', 'data' => null]);

        } catch (\Throwable $th) {
            //throw $th;

            return response()->json(['valid' => false, 'message' => 'error', 'errors' => $th->getMessage()]);

        }

    }

    public function updateEmployer(Request $requets, $id) {

        try {

            $data = Employer::where('id', $id)->update([
                'username' =>  $requets->input('username'),
                'firstname' =>  $requets->input('firstname'),
                'lastname' =>  $requets->input('lastname'),
                'email' =>  $requets->input('email'),
                'birthdate' =>  $requets->input('birthdate'),
                'basic_salary' =>  $requets->input('basic_salary'),
                'group' =>  $requets->input('group'),
                'status' =>  $requets->input('status'),
                'description' =>  $requets->input('description'),
            ]);

            return response()->json(['valid' => true, 'message' => 'success', 'data' => null]);

        } catch (\Throwable $th) {
            //throw $th;

            return response()->json(['valid' => false, 'message' => 'error', 'errors' => $th->getMessage()]);

        }

    }

    public function getbyId($id) {
        $users = Employer::find($id);

        return response()->json(['valid' => true, 'message' => 'data retrieved successfully', 'data' => $users]);
    }

    public function delete($id) {

        try {
            $delete = Employer::where('id', $id)->delete();

            return response()->json(['valid' => true, 'message' => 'success', 'data' => null]);
        } catch (\Throwable $th) {
            return response()->json(['valid' => false, 'message' => 'error', 'errors' => $th->getMessage()]);
        }

    }
    //
}
