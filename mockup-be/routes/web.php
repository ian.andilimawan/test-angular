<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Http\Controllers\EmployerController;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->post('/login', 'UserController@index');
$router->get('/employes', 'EmployerController@index');
$router->post('/employes', 'EmployerController@addEmployer');
$router->put('/employes/{id}', 'EmployerController@updateEmployer');
$router->delete('/employes/{id}', 'EmployerController@delete');
$router->get('/employes/{id}', 'EmployerController@getbyId');
