<?php

namespace Database\Factories;

use App\Models\Employer;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => $this->faker->name,
            'firstname' => $this->faker->firstName(),
            'lastname' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail,
            'birthdate' => $this->faker->date('Y-m-d'),
            'status' => $this->faker->boolean(),
            'basic_salary' => $this->faker->numberBetween($min = 1500, $max = 6000),
            'group' => $this->faker->sentence(),
            'description' => $this->faker->sentence(),

        ];
    }
}
